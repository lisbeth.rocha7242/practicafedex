<?php if ($this->session->flashdata("confirmacion")): ?>
  <script type="text/javascript">
toastr.success("<?php echo $this->session->flashdata("confirmacion"); ?>");
  </script>
  <?php $this->session->set_flashdata("confirmacion","")  ?>
<?php endif; ?>

<?php if ($this->session->flashdata("error")): ?>
  <script type="text/javascript">
toastr.success("<?php echo $this->session->flashdata("error"); ?>");
  </script>
  <?php $this->session->set_flashdata("error","")  ?>
<?php endif; ?>
<style media="screen">
  .obligatorio{
      color: red;

      border-radius: 20px;
      font-size: 12px;
      padding-left: 5px;
      padding-right: 5px;
  }
  .error{
    color: red;
    font-weight: bold;
  }
  input.error{
    border: 2px solid red;
  }
</style>


    <!--hm-footer start-->
    <section class="hm-footer">
      <div class="container">
        <div class="hm-footer-details">
          <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-12">
              <div class="hm-footer-widget">
                <div class="hm-foot-title ">
                  <div class="logo">
                    <a href="index.html">
                      <img src="https://logodownload.org/wp-content/uploads/2014/07/fedex-logo.png" alt="logo" height="15px" width="150px">
                    </a>
                  </div><!-- /.logo-->
                </div><!--/.hm-foot-title-->
                <div class="hm-foot-para">
                  <p>
                    Creemos que una sociedad diversa es una sociedad más fuerte. Nos esforzamos por asegurar que nuestra compañía refleje las muchas culturas que hay en nuestra fuerza de trabajo, nuestros clientes y nuestras comunidades alrededor del mundo.</p>
                </div><!--/.hm-foot-para-->
                <div class="hm-foot-icon">
                  <ul>
                    <li><a href="https://www.facebook.com/FedExLATAM/?brand_redir=150993094945648&locale=es_LA"><i class="fa fa-facebook" aria-hidden="true"></i></a></li><!--/li-->
                    <li><a href="https://www.fedex.com/es-ec/home.html"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li><!--/li-->
                    <li><a href="https://www.instagram.com/fedex/?hl=es"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li><!--/li-->
                    <li><a href="https://twitter.com/fedexteayuda"><i class="fa fa-twitter" aria-hidden="true"></i></a></li><!--/li-->
                  </ul><!--/ul-->
                </div><!--/.hm-foot-icon-->
              </div><!--/.hm-footer-widget-->
            </div><!--/.col-->
            <div class=" col-md-2 col-sm-6 col-xs-12">
              <div class="hm-footer-widget">
                <div class="hm-foot-title">
                  <h4>Servicios Disponibles</h4>
                </div><!--/.hm-foot-title-->
                <div class="footer-menu ">
                  <ul class="">
                    <li><a href="<?php echo site_url(); ?>" >Envios Nacionales</a></li>
                    <li><a href="<?php echo site_url('envios/nosotros'); ?>">Envios Internacionales</a></li>
                    <li><a href="<?php echo site_url('envios/nosotros'); ?>">Envios Locales</a></li>
                    <li><a href="<?php echo site_url('envios/nosotros'); ?>">Envios Provinciales</a></li>

                  </ul>


                </div><!-- /.footer-menu-->
              </div><!--/.hm-footer-widget-->
            </div><!--/.col-->
            <div class=" col-md-3 col-sm-6 col-xs-12">
              <div class="hm-footer-widget">
                <div class="hm-foot-title">
                  <h4>Nuevos Eventos</h4>
                </div><!--/.hm-foot-title-->
                <div class="hm-para-news">
                  <a href="blog_single.html">
                  Entregamos alegría durante las fiestas. Entregamos esperanza a los sobrevivientes de desastres naturales.
                  </a>
                  <span>12th Junio 2023</span>
                </div><!--/.hm-para-news-->
                <div class="footer-line">
                  <div class="border-bottom"></div>
                </div>
                <div class="hm-para-news">
                  <a href="blog_single.html">
                    Entregamos alegría durante las fiestas. Entregamos esperanza a los sobrevivientes de desastres naturales.
                  </a>
                  <span>13th Junio 2023</span>
                </div><!--/.hm-para-news-->
              </div><!--/.hm-footer-widget-->
            </div><!--/.col-->
            <div class=" col-md-3 col-sm-6  col-xs-12">
              <div class="hm-footer-widget">
                <div class="hm-foot-title">
                  <h4>INFORMACIÓN</h4>
                </div><!--/.hm-foot-title-->
                <div class="hm-foot-para">
                  <p class="para-news">
                  Estamos motivados para crear soluciones innovadoras para nuestros clientes y encontrar formas de trabajar de forma más inteligente.
                  </p>
                </div><!--/.hm-foot-para-->
                <div class="hm-foot-email">
                  <div class="foot-email-box">
                    <input type="text" class="form-control" placeholder="Email Address">
                  </div><!--/.foot-email-box-->
                  <div class="foot-email-subscribe">
                    <button type="button" >Visita</button>
                  </div><!--/.foot-email-icon-->
                </div><!--/.hm-foot-email-->
              </div><!--/.hm-footer-widget-->
            </div><!--/.col-->
          </div><!--/.row-->
        </div><!--/.hm-footer-details-->
      </div><!--/.container-->

    </section><!--/.hm-footer-details-->
    <!--hm-footer end-->

    <!-- footer-copyright start -->
    <footer class="footer-copyright">
      <div class="container">
        <div class="row">
          <div class="col-sm-7">
            <div class="foot-copyright pull-left">
              <p>
                &copy; All Rights Reserved. Designed and Developed by
                <a href="https://www.themesine.com">ThemeSINE</a>
              </p>
            </div><!--/.foot-copyright-->
          </div><!--/.col-->
          <div class="col-sm-5">
            <div class="foot-menu pull-right
            ">
              <ul>
                <li ><a href="#">legal</a></li>
                <li ><a href="#">sitemap</a></li>
                <li ><a href="#">privacy policy</a></li>
              </ul>
            </div><!-- /.foot-menu-->
          </div><!--/.col-->
        </div><!--/.row-->
        <div id="scroll-Top">
          <i class="fa fa-angle-double-up return-to-top" id="scroll-top" data-toggle="tooltip" data-placement="top" title="" data-original-title="Back to Top" aria-hidden="true"></i>
        </div><!--/.scroll-Top-->
      </div><!-- /.container-->

    </footer><!-- /.footer-copyright-->
    <!-- footer-copyright end -->



    <!-- jaquery link -->

    <script src="<?php echo base_url('plantilla/assets/js/jquery.js'); ?>"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->

        <!--modernizr.min.js-->
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>


    <!--bootstrap.min.js-->
        <script type="text/javascript" src="<?php echo base_url('plantilla/assets/js/bootstrap.min.js'); ?>"></script>

    <!-- bootsnav js -->
    <script src="<?php echo base_url('plantilla/assets/js/bootsnav.js'); ?>"></script>

    <!-- for manu -->
    <script src="<?php echo base_url('plantilla/assets/js/jquery.hc-sticky.min.js'); ?>" type="text/javascript"></script>


    <!-- vedio player js -->
    <script src="<?php echo base_url('plantilla/assets/js/jquery.magnific-popup.min.js'); ?>"></script>


    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>

        <!--owl.carousel.js-->
        <script type="text/javascript" src="<?php echo base_url('plantilla/assets/js/owl.carousel.min.js'); ?>"></script>

    <!-- counter js -->
    <script src="<?php echo base_url('plantilla/assets/js/jquery.counterup.min.js'); ?>"></script>
    <script src="<?php echo base_url('plantilla/assets/js/jquery.counterup.min.js'); ?>"></script>

        <!--Custom JS-->
        <script type="text/javascript" src="<?php echo base_url('plantilla/assets/js/jak-menusearch.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('plantilla/assets/js/custom.js'); ?>"></script>


    </body>

    </html>
