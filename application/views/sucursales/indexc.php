<body style="background-color:#FFB48A ">


<div class="row">
  <div class="col-md-8" >
    <h1 align="center"> <img src="<?php echo base_url('assets/images/suc.png'); ?>" alt="..." width="100" height="100"><b>LISTADO DE SUCURSALES </b></h1>


  </div>
  <br>
  <br>

  <div class="col-md-4">
    <a href="<?php echo site_url('sucursales/nuevoc'); ?>" class="btn btn-primary">
      <i class="glyphicon glyphicon-plus"></i>
    Agregar Sucursal</a>
  </div>
</div>
<br>
<?php if ($sucursal): ?>
  <table class="table table-striped table-bordered" style="background-color: white;">
    <thead>
      <tr>
        <th>ID</th>
        <th>NOMBRE</th>
        <th>CONTINENTE</th>
        <th>CALLE</th>
        <th>PISO</th>
        <th>TELÉFONO</th>
        <th>ENCARGADO</th>
        <th>LATITUD</th>
        <th>LONGITUD</th>
        <th>ACCIONES</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($sucursal as $filaTemporal): ?>
        <tr>
          <td>
           <?php echo $filaTemporal->id_suc; ?>
          </td>

          <td>
           <?php echo $filaTemporal->nombre_suc; ?>
          </td>
          <td>
           <?php echo $filaTemporal->continente_suc; ?>
          </td>
          <td>
           <?php echo $filaTemporal->calle_suc; ?>
          </td>
          <td>
           <?php echo $filaTemporal->piso_suc; ?>
          </td>
          <td>
           <?php echo $filaTemporal->telefono_suc; ?>
          </td>
          <td>
           <?php echo $filaTemporal->encargado_suc; ?>
          </td>
          <td>
           <?php echo $filaTemporal->latitud_suc; ?>
          </td>
          <td>
           <?php echo $filaTemporal->longitud_suc; ?>
          </td>


          <td class="text-center">
              <a href="<?php echo site_url('/sucursales/editar/'); ?><?php echo $filaTemporal->id_suc;?>" title="Editar Sucursal" onclick="return confirm('¿Estas Seguro de editar la sucursal?');"style="color:blue;">
            <i class="glyphicon glyphicon-pencil"></i>Editar
            </a>
            &nbsp; &nbsp; &nbsp;
            <a href="<?php echo site_url(); ?>/sucursales/eliminar/<?php echo $filaTemporal->id_suc;?>" title="Eliminar Sucursal" onclick="return confirm('¿Estas Seguro de eliminar el registro de la Sucursal?');"style="color:red">
            <i class="glyphicon glyphicon-trash"></i>Eliminar
            </a>
          </td>

        </tr>

      <?php endforeach; ?>
    </tbody>
  </table>

<?php else: ?>
  <h1>No hay Datos</h1>
<?php endif; ?>
</body>
