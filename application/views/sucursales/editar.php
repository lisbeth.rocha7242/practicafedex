<br>
<body style="background-color:#FFB48A ;">

<h1 align="center"> <img src="<?php echo base_url('assets/images/suc.png'); ?>" alt="..." width="100" height="100"><b>EDITAR SUCURSAL</b></h1>
<br>
<form class=""
id="frm_editar_sucursal"
action="<?php echo site_url('sucursales/procesarActualizacion'); ?>"
method="post">
<div class="container">
    <input type="hidden" name="id_suc" id="suc" class="form-control" value="<?php echo $sucursalEditar->id_suc; ?>">
    <div class="row">
             <div class="col-md-4">
                 <label for="">Nombre Sucursal:</label>
                 <span class="obligatorio">(Obligatorio)</span>
                 <br>
                 <input type="text"
                 placeholder="Ingrese el nombre"
                 class="form-control" required
                 name="nombre_suc" value="<?php echo $sucursalEditar->nombre_suc; ?>"
                 id="nombre_suc">
             </div>
             <div class="col-md-4">
                   <label for="">Continente:</label>
                   <span class="obligatorio">(Obligatorio)</span>
                   <br>
                   <select class="form-control" required name="continente_suc" id="continente_suc">
                     <option value="<?php echo $sucursalEditar->continente_suc; ?>">ASIA</option>
                     <option value="<?php echo $sucursalEditar->continente_suc; ?>">AMÉRICA</option>
                     <option value="<?php echo $sucursalEditar->continente_suc; ?>">ÁFRICA</option>
                     <option value="<?php echo $sucursalEditar->continente_suc; ?>">ANTÁRTIDA</option>
                     <option value="<?php echo $sucursalEditar->continente_suc; ?>">EUROPA</option>
                     <option value="<?php echo $sucursalEditar->continente_suc; ?>">OCEANÍA</option>

                   </select>

               </div>
             <div class="col-md-4">
               <label for="">Calle:</label>
               <span class="obligatorio">(Obligatorio)</span>
               <br>
               <input type="text"
               placeholder="Ingrese el nombre calle"
               class="form-control" required
               name="calle_suc" value="<?php echo $sucursalEditar->calle_suc; ?>"
               id="calle_suc">
             </div>

      </div>


  </div>
    <br>
    <div class="container">
    <div class="row">
    <div class="col-md-4">
        <label for="">Número de Piso:</label>
        <span class="obligatorio">(Obligatorio)</span>
        <br>
        <input type="text"
        placeholder="Ingrese el número de piso"
        class="form-control" required
        name="piso_suc" value="<?php echo $sucursalEditar->piso_suc; ?>"
        id="piso_suc">
    </div>
    <div class="col-md-4">
        <label for="">Teléfono:</label>
        <span class="obligatorio">(Obligatorio)</span>
        <br>
        <input type="number"
        placeholder="Ingrese el Teléfono"
        class="form-control" required
        name="telefono_suc" value="<?php echo $sucursalEditar->telefono_suc; ?>"
        id="telefono_suc">

    </div>


    <div class="col-md-4">
        <label for="">Encargado por:</label>
        <span class="obligatorio">(Obligatorio)</span>
        <br>
        <input type="text"
        placeholder="Ingrese el nombre del Encargado"
        class="form-control" required
        name="encargado_suc" value="<?php echo $sucursalEditar->encargado_suc; ?>"
        id="encargado_suc">
    </div>

        </div>
        </div>
        <br>

        <div class="container">
      <div class="row">
        <div class="col-md-4">
            <label for="">Latitud:</label>
            <span class="obligatorio">(Obligatorio)</span>
            <br>
            <input type="float"
            placeholder="Ingrese la Latitud"
            class="form-control" readonly
            required
            name="latitud_suc" value="<?php echo $sucursalEditar->latitud_suc; ?>"
            id="latitud_suc">
        </div>
        <div class="col-md-4">
            <label for="">Longitud:</label>
            <span class="obligatorio">(Obligatorio)</span>
            <br>
            <input type="float"
            placeholder="Ingrese la longitud"
            class="form-control" readonly
            required
            name="longitud_suc" value="<?php echo $sucursalEditar->longitud_suc; ?>"
            id="longitud_suc">

        </div>


        </div>
<br>
<div class="row">
  <div class="col-md-12">
    <div id="mapaUbicacion" style="height:500px;width:100%;border:2px solid black;">

    </div>

  </div>

</div>
    </div>

  <script type="text/javascript">
     function initMap(){
       var centro=new google.maps.LatLng(<?php echo $sucursalEditar->latitud_suc; ?>,<?php echo $sucursalEditar->longitud_suc; ?>);

       var mapa1=new google.maps.Map(
          document.getElementById('mapaUbicacion'),
          {
            center:centro,
            zoom:7,
            mapTypeId:google.maps.MapTypeId.ROADMAP
          }
       );
       var marcador=new google.maps.Marker({
         position:centro,
         map:mapa1,
         title:"Seleccione su direccion",
         icon:"<?php echo base_url('assets/images/sucur.png'); ?>",
         draggable:true
       });
       google.maps.event.addListener(marcador,'dragend',function(event){
         //alert("Se termino el Drag");
         document.getElementById('latitud_suc').value=
         this.getPosition().lat();
         document.getElementById('longitud_suc').value=
         this.getPosition().lng();
       });

     }//cierre de la funcion
  </script>



    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
              Guardar
            </button>
            &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp;
            <a href="<?php echo site_url();?>/sucursales/indexc"
              class="btn btn-danger">
              Cancelar
            </a>
        </div>
    </div>
</div>
</form>
</body>
<br>
<br>
<script type="text/javascript">
  $("#frm_editar_sucursal").validate({
    rules:{
      nombre_suc:{
        required:true,
        minlength:3,
        maxlength:250,
        letras:true,
      },
      calle_suc:{
        required:true,
        minlength:3,
        maxlength:250,
        letras:true,
      },
      piso_suc:{
        required:true,
        minlength:1,
        maxlength:3,
        digits:true,
      },
      telefono_suc:{
        required:true,
        minlength:10,
        maxlength:10,
        digits:true,
      },
      continente_suc:{
        required:true,
      },
      encargado_suc:{
        required:true,
        minlength:3,
        maxlength:250,
        letras:true,
      },
      latitud_suc:{
        required:true,
      },
      longitud_suc:{
        required:true,
      }

    },
    messages:{
      nombre_suc:{
        required:"Por favor ingrese letras",
        minlength:"EL nombre debe tener al menos 3 letras",
        maxlength:"EL nombre debe tener al menos 15 letras",
      },
      calle_suc:{
        required:"Por favor ingrese letras",
        minlength:"EL nombre debe tener al menos 3 letras",
        maxlength:"EL nombre debe tener al menos 15 letras",
      },
      piso_suc:{
        required:"Por favor ingrese números",
        minlength:"Incorrecto, escriba al menos un digito",
        maxlength:"Incorrecto, No existe gran cantidad de pisos",
      },
      telefono_suc:{
        required:"Por favor ingrese numeros",
        minlength:"Telefono incorrecto, ingrese 10 digitos",
        maxlength:"Telefono incorrecto, ingrese 10 digitos",
      },
      encargado_suc:{
        required:"Por favor ingrese letras",
        minlength:"EL nombre debe tener al menos 3 letras",
        maxlength:"EL nombre debe tener al menos 15 letras",
      },
      latitud_suc:{
        required:"Por favor arrastre en el mapa su ubicación",
      },
      longitud_suc:{
        required:"Por favor arrastre en el mapa su ubicación",
      }
    }
  });
</script>
