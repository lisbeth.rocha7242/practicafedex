<br>
<br>
<body style="background-color:#A7CBEE ;">


<h1 align="center"> <img src="https://cdn-icons-png.flaticon.com/512/476/476742.png" alt="..." width="100" height="100"><b>NUEVO CLIENTE</b></h1>
<br>
<form class=""
id="frm_nuevo_cliente"
action="<?php echo site_url(); ?>/Clientes/guardar"
method="post">
<div class="container">
    <div class="row">
             <div class="col-md-4">
                 <label for="">Cédula:</label>
                 <span class="obligatorio">(Obligatorio)</span>
                 <br>
                 <input type="number"
                 placeholder="Ingrese su cédula"
                 class="form-control"
                 required
                 name="cedula_cli" value=""
                 id="cedula_cli">
             </div>
             <div class="col-md-4">
               <label for="">Nombres:</label>
               <span class="obligatorio">(Obligatorio)</span>
               <br>
               <input type="text"
               placeholder="Ingrese el nombre"
               class="form-control" required
               name="nombre_cli" value=""
               id="nombre_cli">
             </div>
             <div class="col-md-4">
                 <label for="">Apellidos:</label>
                 <span class="obligatorio">(Obligatorio)</span>
                 <br>
                 <input type="text"
                 placeholder="Ingrese el apellido"
                 class="form-control" required
                 name="apellido_cli" value=""
                 id="apellido_cli">
             </div>

      </div>


  </div>
    <br>
    <div class="container">
    <div class="row">
    <div class="col-md-4">
        <label for="">Email:</label>
        <span class="obligatorio">(Obligatorio)</span>
        <br>
        <input type="email"
        placeholder="Ingrese su Email"
        class="form-control" required
        name="email_cli" value=""
        id="email_cli">

    </div>
    <div class="col-md-4">
        <label for="">Ciudad:</label>
        <span class="obligatorio">(Obligatorio)</span>
        <br>
        <input type="text"
        placeholder="Ingrese su ciudad"
        class="form-control" required
        name="ciudad_cli" value=""
        id="ciudad_cli">
    </div>
    <div class="col-md-4">
        <label for="">País:</label>
        <span class="obligatorio">(Obligatorio)</span>
        <br>
        <input type="text"
        placeholder="Ingrese su Pais"
        class="form-control" required
        name="pais_cli" value=""
        id="pais_cli">
    </div>

        </div>
        </div>
        <br>

        <div class="container">
      <div class="row">
        <div class="col-md-4">
            <label for="">Teléfono:</label>
            <span class="obligatorio">(Obligatorio)</span>
            <br>
            <input type="number"
            placeholder="Ingrese su Teléfono"
            class="form-control" required
            name="telefono_cli" value=""
            id="telefono_cli">
        </div>
        <div class="col-md-4">
            <label for="">Latitud:</label>
            <span class="obligatorio">(Obligatorio)</span>
            <br>
            <input type="number"
            placeholder="Ingrese la latitud"
            class="form-control" readonly
            required
            name="latitud_cli" value=""
            id="latitud_cli">

        </div>
        <div class="col-md-4">
            <label for="">Longitud:</label>
            <span class="obligatorio">(Obligatorio)</span>
            <br>
            <input type="number"
            placeholder="Ingrese la longitud"
            class="form-control" readonly
            required
            name="longitud_cli" value=""
            id="longitud_cli">

        </div>

        </div>
<br>
<div class="row">
  <div class="col-md-12">
    <div id="mapaUbicacion" style="height:500px;width:100%;border:2px solid black;">

    </div>

  </div>

</div>
    </div>

  <script type="text/javascript">
     function initMap(){
       var centro=new google.maps.LatLng(-0.9103118368246511,-78.6288056178432);

       var mapa1=new google.maps.Map(
          document.getElementById('mapaUbicacion'),
          {
            center:centro,
            zoom:7,
            mapTypeId:google.maps.MapTypeId.ROADMAP
          }
       );
       var marcador=new google.maps.Marker({
         position:centro,
         map:mapa1,
         title:"Seleccione su direccion",
         icon:"<?php echo base_url('assets/images/cliente.png'); ?>",
         draggable:true
       });
       google.maps.event.addListener(marcador,'dragend',function(event){
         //alert("Se termino el Drag");
         document.getElementById('latitud_cli').value=
         this.getPosition().lat();
         document.getElementById('longitud_cli').value=
         this.getPosition().lng();
       });

     }//cierre de la funcion
  </script>



    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
              Guardar
            </button>
            &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp;
            <a href="<?php echo site_url();?>/clientes/index"
              class="btn btn-danger">
              Cancelar
            </a>
        </div>
    </div>
</div>
</form>




</body>
<br>
<br>
<script type="text/javascript">
  $("#frm_nuevo_cliente").validate({
    rules:{
      cedula_cli:{
        required:true,
        minlength:10,
        maxlength:10,
        digits:true,
      },
      nombre_cli:{
        required:true,
        minlength:3,
        maxlength:250,
        letras:true,

      },
      apellido_cli:{
        required:true,
        minlength:3,
        maxlength:250,
        letras:true,
      },
      email_cli:{
        required:true,
        email:true,
      },
      ciudad_cli:{
        required:true,
        minlength:3,
        maxlength:250,
        letras:true,
      },
      pais_cli:{
        required:true,
        minlength:3,
        maxlength:250,
        letras:true,
      },
      telefono_cli:{
        required:true,
        minlength:10,
        maxlength:10,
        digits:true,
      },
      latitud_cli:{
        required:true,
      },
      longitud_cli:{
        required:true,
      }

    },
    messages:{
      cedula_cli:{
        required:"Por favor ingrese el número de cédula",
        minlength:"Cédula Incorrecta, ingrese 10 digitos",
        maxlength:"Cédula Incorrecta, ingrese 10 digitos",
        digits:"Solo se aceptan números",
        number:"Solo se aceptan números",
      },
      nombre_cli:{
        required:"Por favor ingrese letras",
        minlength:"EL nombre debe tener al menos 3 letras",
        maxlength:"EL nombre debe tener al menos 15 letras",

      },
      apellido_cli:{
        required:"Por favor ingrese letras",
        minlength:"EL apellido debe tener al menos 3 letras",
        maxlength:"EL apellido debe tener al menos 15 letras",
      },
      email_cli:{
        required:"Por favor ingrese un email válido",
        email:"Incorrecto, ingrese un email válido",
      },
      ciudad_cli:{
        required:"Por favor ingrese letras",
        minlength:"EL nombre debe tener al menos 3 letras",
        maxlength:"Debe tener al menos 15 letras",
      },
      pais_cli:{
        required:"Por favor ingrese letras",
        minlength:"EL nombre debe tener al menos 3 letras",
        maxlength:"Debe tener al menos 15 letras",
      },
      telefono_cli:{
        required:"Por favor ingrese solo números",
        minlength:"Teléfono incorrecto, ingrese 10 digitos",
        maxlength:"Teléfono incorrecto, ingrese 10 digitos",
      },
      latitud_cli:{
        required:"Por favor arrastre en el mapa su ubicación",
      },
      longitud_cli:{
        required:"Por favor arrastre en el mapa su ubicación",
      }

    }
  });
</script>
