<div class="row">
  <div class="col-md-8" >
    <h1 align="center"> <img src="https://images.vexels.com/media/users/3/157512/isolated/preview/d737a872708b488d89d0341ac9b8bc5a-personas-contacto-icono-personas.png" alt="..." width="100" height="100"><b>LISTADO DE CLIENTES</b></h1>


  </div>
  <br>
  <br>

  <div class="col-md-4">
    <a href="<?php echo site_url('clientes/nuevo'); ?>" class="btn btn-primary">
      <i class="glyphicon glyphicon-plus"></i>
    Agregar Cliente</a>
  </div>
</div>
<br>
<?php if ($clientes): ?>
  <table class="table table-striped table-bordered" style="background-color: white;">
    <thead>
      <tr>
        <th>ID</th>
        <th>CÉDULA</th>
        <th>NOMBRE</th>
        <th>APELLIDO</th>
        <th>EMAIL</th>
        <th>CIUDAD</th>
        <th>PAÍS</th>
        <th>TELÉFONO</th>
        <th>LATITUD</th>
        <th>LONGITUD</th>
        <th>ACCIONES</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($clientes as $filaTemporal): ?>
        <tr>
          <td>
           <?php echo $filaTemporal->id_cli ?>
          </td>

          <td>
           <?php echo $filaTemporal->cedula_cli ?>
          </td>
          <td>
           <?php echo $filaTemporal->nombre_cli ?>
          </td>
          <td>
           <?php echo $filaTemporal->apellido_cli ?>
          </td>
          <td>
           <?php echo $filaTemporal->email_cli ?>
          </td>
          <td>
           <?php echo $filaTemporal->ciudad_cli ?>
          </td>
          <td>
           <?php echo $filaTemporal->pais_cli ?>
          </td>
          <td>
           <?php echo $filaTemporal->telefono_cli ?>
          </td>
          <td>
           <?php echo $filaTemporal->latitud_cli ?>
          </td>
          <td>
           <?php echo $filaTemporal->longitud_cli ?>
          </td>



          <td class="text-center">

            <a href="<?php echo site_url(); ?>/pedidos/nuevoa/" title="Realizar Pedido" style="color:#27893B ">
            <i class="glyphicon glyphicon-shopping-cart"></i>Envio
            </a>
              &nbsp; &nbsp; &nbsp;
              <a href="<?php echo site_url('/clientes/editar/'); ?><?php echo $filaTemporal->id_cli;?>" title="Editar Cliente" onclick="return confirm('¿Estas Seguro de editar el registro cliente?');">
<<<<<<< HEAD
                <i class="glyphicon glyphicon-pencil"></i>Editar
=======
                <i class="glyphicon glyphicon-pencil"></i>
>>>>>>> 9935d28440230108117e3b01a0ceaf71770dafea
              </a>


            &nbsp; &nbsp; &nbsp;
            <a href="<?php echo site_url(); ?>/clientes/eliminar/<?php echo $filaTemporal->id_cli;?>" title="Eliminar Cliente" onclick="return confirm('¿Estas Seguro de eliminar el registro cliente?');"style="color:red">
            <i class="glyphicon glyphicon-trash"></i>Eliminar
            </a>
          </td>

        </tr>

      <?php endforeach; ?>
    </tbody>
  </table>

<?php else: ?>
  <h1>No hay Datos</h1>
<?php endif; ?>
</body>
