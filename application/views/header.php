<!doctype html>
<html class="no-js" lang="en">

    <head>
        <!-- META DATA -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->



        <!--font-family-->
		<link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i,900,900i" rel="stylesheet">

		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900" rel="stylesheet">

		<link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900" rel="stylesheet">

    <!--Importar API-->
     <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBATSumqnJTuJpeJ1KZgkIMllydVtN5qZI&callback=initMap">
     </script>


        <!-- TITLE OF SITE -->
        <title>Fedex</title>

        <!-- for title img -->
		<link rel="shortcut icon" type="image/icon" href="<?php echo base_url('plantilla/assets/images/logo/favicon.png'); ?>"/>

        <!--font-awesome.min.css-->
        <link rel="stylesheet" href="<?php echo base_url('plantilla/assets/css/font-awesome.min.css'); ?>">

		<!--linear icon css-->
		<link rel="stylesheet" href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css">

		<!--animate.css-->
        <link rel="stylesheet" href="<?php echo base_url('plantilla/assets/css/animate.css'); ?>">

		<!--hover.css-->
        <link rel="stylesheet" href="<?php echo base_url('plantilla/assets/css/hover-min.css'); ?>">

		<!--vedio player css-->
        <link rel="stylesheet" href="<?php echo base_url('plantilla/assets/css/magnific-popup.css'); ?>">

		<!--owl.carousel.css-->
        <link rel="stylesheet" href="<?php echo base_url('plantilla/assets/css/owl.carousel.min.css'); ?>">
		<link href="<?php echo base_url('plantilla/assets/css/owl.theme.default.min.css') ?>" rel="stylesheet"/>


        <!--bootstrap.min.css-->
        <link rel="stylesheet" href="<?php echo base_url('plantilla/assets/css/bootstrap.min.css'); ?>">

		<!-- bootsnav -->
		<link href="<?php echo base_url('plantilla/assets/css/bootsnav.css') ?>" rel="stylesheet"/>

        <!--style.css-->
        <link rel="stylesheet" href="<?php echo base_url('plantilla/assets/css/style.css'); ?>">

        <!--responsive.css-->
        <link rel="stylesheet" href="<?php echo base_url('plantilla/assets/css/responsive.css'); ?>">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

        <!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!--IMPORTACION JQUERY-->
        <script src="https://code.jquery.com/jquery-3.7.0.min.js" integrity="sha256-2Pmvv0kuTBOenSvLm6bvfBSSHrUJ+3A7x6P5Ebd07/g=" crossorigin="anonymous"></script>
        <!-- IMPORTACION DE JQUERY VALIDATE-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.min.js" integrity="sha512-rstIgDs0xPgmG6RX1Aba4KV5cWJbAMcvRCVmglpam9SoHZiUCyQVDdH2LPlxoHtrv17XWblE/V/PP+Tr04hbtA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

        <!-- VALIDACION DEL CAMPO DE SOLO INGRESAR LETRAS-->
        <script type="text/javascript">
          jQuery.validator.addMethod("letras", function(value, element) {
            //return this.optional(element) || /^[a-z]+$/i.test(value);
            return this.optional(element) || /^[A-Za-zÁÉÍÑÓÚáéí ñóú]*$/.test(value);

          }, "Este campo solo acepta letras");
        </script>
        <!-- IMPORTACION DE la libreria  DE TOASTR-->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css" integrity="sha512-3pIirOrwegjM6erE5gPSwkUzO+3cTjpnV9lexlNZqvupR64iZBnOOTiiLPb9M36zpMScbmUNIcHUqKD47M719g==" crossorigin="anonymous" referrerpolicy="no-referrer" />


    </head>

	<body>
		<!--[if lte IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->




		<!--header start-->
		<section class="header">
			<div class="container">
				<div class="header-left">
					<ul class="pull-right">
            <li>
							<a href="#">
								Contáctanos:
							</a>
						</li>
						<li>
							<a href="#">
								<i class="fa fa-phone" aria-hidden="true"></i> +992 563 542
							</a>
						</li><!--/li-->
						<li>
							<a href="#">
								<i class="fa fa-envelope" aria-hidden="true"></i>fedexo@mail.com
							</a>
						</li><!--/li-->
            <li>

              <div class=" social-icon">
								<ul>
									<li><a href="https://www.facebook.com/FedExLATAM/?brand_redir=150993094945648&locale=es_LA"><i class="fa fa-facebook"></i></a></li>
									<li><a href="https://www.fedex.com/es-ec/home.html"><i class="fa fa-google-plus"></i></a></li>
									<li><a href="https://www.instagram.com/fedex/?hl=es"><i class="fa fa-linkedin"></i></a></li>
									<li><a href="https://twitter.com/fedexteayuda"><i class="fa fa-twitter"></i></a></li>
								</ul><!--/.ul -->
							</div><!--/.social-icon -->
						</li><!--/li -->

					</ul><!--/ul-->
				</div><!--/.header-left -->
      </div>



          </section>


		<!--menu start-->
		<section id="menu">
			<div class="container">
				<div class="menubar">
					<nav class="navbar navbar-default">

						<!-- Brand and toggle get grouped for better mobile display -->
						<div class="navbar-header">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
							<a class="navbar-brand" href="<?php echo site_url(); ?>">
								<img src="https://logodownload.org/wp-content/uploads/2014/07/fedex-logo.png" alt="logo" height="15px" width="150px">
							</a>
						</div><!--/.navbar-header -->

						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav navbar-right">

                <li><a href="<?php echo site_url(); ?>" >Bienvenidos</a></li>
                <li><a href="<?php echo site_url('envios/nosotros'); ?>">Quienes Somos</a></li>
                <li><a href="<?php echo site_url('envios/historias'); ?>">Historia</a></li>
                <li><a href="<?php echo site_url('envios/servicios'); ?>">Servicios</a></li>


        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Clientes <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a style="color:black" href="<?php echo site_url('/clientes/nuevo');?>">Nuevos Clientes</a></li>
            <li><a style="color:black" href="<?php echo site_url('/clientes/index');?>">Listado Clientes</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Pedidos <span class="caret"></span></a>
          <ul class="dropdown-menu">

            <li><a style="color:black" href="<?php echo site_url('/pedidos/indexa');?>">Listado Pedidos</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Sucursales <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a style="color:black" href="<?php echo site_url('/sucursales/nuevoc');?>">Nuevos Sucursales</a></li>
            <li><a style="color:black" href="<?php echo site_url('/sucursales/indexc');?>">Listado Sucursales</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Reportes <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a style="color:black" href="<?php echo site_url('/reportes/clientes');?>">Clientes</a></li>
            <li><a style="color:black" href="<?php echo site_url('/reportes/pedidos');?>">Pedidos</a></li>
            <li><a style="color:black" href="<?php echo site_url('/reportes/sucursales');?>">Sucursales</a></li>
            <li><a style="color:black" href="<?php echo site_url('/reportes/general');?>">General</a></li>
          </ul>
        </li>
      </ul>
      </ul>



							</ul><!-- / ul -->
						</div><!-- /.navbar-collapse -->
					</nav><!--/nav -->
				</div><!--/.menubar -->
			</div><!-- /.container -->

		</section><!--/#menu-->
		<!--menu end-->
