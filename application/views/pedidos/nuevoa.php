<br>
<body style="background-color:#F3BAF9 ;">

<h1 align="center"> <img src="<?php echo base_url('assets/images/pedi.png'); ?>" alt="..." width="100" height="100"> &nbsp;  <b>NUEVO PEDIDO</b></h1>
<br>
<form class=""
id="frm_nuevo_pedido"
action="<?php echo site_url(); ?>/Pedidos/guardar"
method="post">
<br>
<div class="container">
            <div class="row">
              <div class="col-md-4">
                  <label for="">Nombre:</label>
                  <span class="obligatorio">(Obligatorio)</span>
                  <br>
                  <input type="text"
                  placeholder="Ingrese nombre del pedido"
                  class="form-control" required
                  name="nombre_ped" value=""
                  id="nombre_ped">
              </div>
             <div class="col-md-4">
                 <label for="">Fecha de Envio:</label>
                 <span class="obligatorio">(Obligatorio)</span>
                 <br>
                 <input type="date"
                 placeholder="Ingrese Fecha de Envio"
                 class="form-control" required
                 name="fechaEnvio_ped" value=""
                 id="fechaEnvio_ped">
             </div>
             <div class="col-md-4">
               <label for="">Cantidad:</label>
               <span class="obligatorio">(Obligatorio)</span>
               <br>
               <input type="number"
               placeholder="Ingrese la cantidad"
               class="form-control" required
               name="cantidad_ped" value=""
               id="cantidad_ped">
             </div>

      </div>


  </div>
    <br>
    <div class="container">
    <div class="row">
      <div class="col-md-4">
          <label for="">Origen:</label>
          <span class="obligatorio">(Obligatorio)</span>
          <br>
          <input type="text"
          placeholder="Ingrese el origen"
          class="form-control" required
          name="origen_ped" value=""
          id="origen_ped">
      </div>
    <div class="col-md-4">
        <label for="">Destino:</label>
        <span class="obligatorio">(Obligatorio)</span>
        <br>
        <input type="text"
        placeholder="Ingrese el destino"
        class="form-control" required
        name="destino_ped" value=""
        id="destino_ped">

    </div>
    <div class="col-md-4">
        <label for="">Recibido por:</label>
        <span class="obligatorio">(Obligatorio)</span>
        <br>
        <input type="text"
        placeholder="Ingrese el nombre del que va a recibir el pedido"
        class="form-control" required
        name="recibido_ped" value=""
        id="recibido_ped">
    </div>
        </div>
        </div>
        <br>

        <div class="container">
      <div class="row">
        <div class="col-md-4">
            <label for="">Fecha de Entrega:</label>
            <span class="obligatorio">(Obligatorio)</span>
            <br>
            <input type="date"
            placeholder="Ingrese la fecha de Entrega"
            class="form-control" required
            name="fechaEntrega_ped" value=""
            id="fechaEntrega_ped">
        </div>
        <div class="col-md-4">
            <label for="">Latitud:</label>
            <span class="obligatorio">(Obligatorio)</span>
            <br>
            <input type="float"
            placeholder="Ingrese su Latitud"
            class="form-control" readonly
            required
            name="latitud_ped" value=""
            id="latitud_ped">
        </div>
        <div class="col-md-4">
            <label for="">Longitud:</label>
            <span class="obligatorio">(Obligatorio)</span>
            <br>
            <input type="float"
            placeholder="Ingrese la longitud"
            class="form-control" readonly
            required
            name="longitud_ped" value=""
            id="longitud_ped">

        </div>


        </div>
<br>
<div class="row">
  <div class="col-md-12">
    <div id="mapaUbicacion" style="height:500px;width:100%;border:2px solid black;">

    </div>

  </div>

</div>
    </div>

  <script type="text/javascript">
     function initMap(){
       var centro=new google.maps.LatLng(-0.9103118368246511,-78.6288056178432);

       var mapa1=new google.maps.Map(
          document.getElementById('mapaUbicacion'),
          {
            center:centro,
            zoom:7,
            mapTypeId:google.maps.MapTypeId.ROADMAP
          }
       );
       var marcador=new google.maps.Marker({
         position:centro,
         map:mapa1,
         title:"Seleccione su direccion",
         icon:"<?php echo base_url('assets/images/pedi.png'); ?>",
         draggable:true
       });
       google.maps.event.addListener(marcador,'dragend',function(event){
         //alert("Se termino el Drag");
         document.getElementById('latitud_ped').value=
         this.getPosition().lat();
         document.getElementById('longitud_ped').value=
         this.getPosition().lng();
       });

     }//cierre de la funcion
  </script>



    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
              Guardar
            </button>
            &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp;
            <a href="<?php echo site_url();?>/pedidos/indexa"
              class="btn btn-danger">
              Cancelar
            </a>
        </div>
    </div>
</div>
</form>
</body>
<br>
<br>
<script type="text/javascript">
  $("#frm_nuevo_pedido").validate({
    rules:{
      nombre_ped:{
        required:true,
        minlength:3,
        maxlength:200,
        letras:true,
      },
      fechaEnvio_ped:{
        required:true,
        date:true,
      },
      cantidad_ped:{
        required:true,
        minlength:1,
        maxlength:10,
        digits:true,
      },
      origen_ped:{
        required:true,
        minlength:3,
        maxlength:250,
        letras:true,
      },
      destino_ped:{
        required:true,
        minlength:3,
        maxlength:250,
        letras:true,
      },
      recibido_ped:{
        required:true,
        minlength:3,
        maxlength:250,
        letras:true,
      },
      fechaEntrega_ped:{
        required:true,
        date:true,
      },
      latitud_cli:{
        required:true,
      },
      longitud_cli:{
        required:true,
      }

    },
    messages:{
      nombre_ped:{
        required:"Por favor ingrese letras",
        minlength:"EL nombre debe tener al menos 3 letras",
        maxlength:"EL nombre debe tener al menos 15 letras",
      },
      fechaEnvio_ped:{
        required:"Por favor seleccione la fecha",
        date:"Seleccione la fecha dd/mm/aaaa",
      },
      cantidad_ped:{
        required:"Por favor ingrese numeros",
        minlength:"Incorrecto, Escriba al menos un número",
        maxlength:"Exceso de de pedidos",
      },
      origen_ped:{
        required:"Por favor ingrese letras",
        minlength:"EL nombre debe tener al menos 3 letras",
        maxlength:"Debe tener al menos 15 letras",
      },
      destino_ped:{
        required:"Por favor ingrese letras",
        minlength:"EL nombre debe tener al menos 3 letras",
        maxlength:"Debe tener al menos 15 letras",
      },
      recibido_ped:{
        required:"Por favor ingrese letras",
        minlength:"EL nombre debe tener al menos 3 letras",
        maxlength:"Debe tener al menos 15 letras",
      },
      fechaEntrega_ped:{
        required:"Por favor selecione la fecha",
        date:"Seleccione la fecha dd/mm/aaaa",
      },
      latitud_ped:{
        required:"Por favor arrastre en el mapa su ubicación",
      },
      longitud_ped:{
        required:"Por favor arrastre en el mapa su ubicación",
      }

    }
  });
</script>
