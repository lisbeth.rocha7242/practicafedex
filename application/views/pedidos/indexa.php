<body style="background-color:#F3BAF9">


<div class="row">
  <div class="col-md-8" >
    <h1 align="center"> <img src="https://images.vexels.com/media/users/3/157512/isolated/preview/d737a872708b488d89d0341ac9b8bc5a-personas-contacto-icono-personas.png" alt="..." width="100" height="100"><b>LISTADO DE PEDIDOS</b></h1>


  </div>
  <br>
  <br>

  <div class="col-md-4">
    <a href="<?php echo site_url('pedidos/nuevoa'); ?>" class="btn btn-primary">
      <i class="glyphicon glyphicon-plus"></i>
    Agregar Pedido al Carrito </a>
  </div>
</div>
<br>
<?php if ($pedido): ?>
  <table class="table table-striped table-bordered" style="background-color: white;">
    <thead>
      <tr>
        <th>ID</th>
        <th>FECHA DE ENVIO</th>
        <th>CANTIDAD</th>
        <th>ORIGEN</th>
        <th>DESTINO</th>
        <th>RECIBIDO</th>
        <th>FECHA DE ENTREGA</th>
        <th>LATITUD</th>
        <th>LONGITUD</th>
        <th>ACCIONES</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($pedido as $filaTemporal): ?>
        <tr>
          <td>
           <?php echo $filaTemporal->id_ped; ?>
          </td>

          <td>
           <?php echo $filaTemporal->fechaEnvio_ped; ?>
          </td>
          <td>
           <?php echo $filaTemporal->cantidad_ped; ?>
          </td>
          <td>
           <?php echo $filaTemporal->origen_ped; ?>
          </td>
          <td>
           <?php echo $filaTemporal->destino_ped; ?>
          </td>
          <td>
           <?php echo $filaTemporal->recibido_ped; ?>
          </td>
          <td>
           <?php echo $filaTemporal->fechaEntrega_ped; ?>
          </td>
          <td>
           <?php echo $filaTemporal->latitud_ped; ?>
          </td>
          <td>
           <?php echo $filaTemporal->longitud_ped; ?>
          </td>



          <td class="text-center">
            <a href="<?php echo site_url('/pedidos/editar/'); ?><?php echo $filaTemporal->id_ped;?>" title="Editar Pedido" onclick="return confirm('¿Estas Seguro de editar el registro de Pedido?');"style="color:blue;">
            <i class="glyphicon glyphicon-pencil"></i>Editar
            </a>
            &nbsp; &nbsp; &nbsp;
            <a href="<?php echo site_url(); ?>/pedidos/eliminar/<?php echo $filaTemporal->id_ped;?>" title="Eliminar Pedido" onclick="return confirm('¿Estas Seguro de eliminar el registro de pedido?');"style="color:red">
            <i class="glyphicon glyphicon-trash"></i>Eliminar
            </a>
          </td>

        </tr>

      <?php endforeach; ?>
    </tbody>
  </table>

<?php else: ?>
  <h1>No hay Datos</h1>
<?php endif; ?>
</body>
