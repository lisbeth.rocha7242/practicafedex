<!-- header-slider-area start -->
<section class="header-slider-area">
  <div id="carousel-example-generic" class="carousel slide carousel-fade" data-ride="carousel">

    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
      <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <div class="single-slide-item slide-1">

          <div class="container">
            <div class="row">
              <div class="col-sm-12">
                <div class="single-slide-item-content">
                  <h2>FedEx <br> Tu mejor opción</h2>
                  <p>
                  Ya sea que tus envíos sean pesados o livianos, urgentes o no tan sensibles al tiempo, FedEx tiene la solución para ti — con tarifas competitivas por servicios confiables para hacer llegar tus envíos a tiempo a su destino..
                  </p>
                  <button type="button" class="slide-btn">
                  <a href="<?php echo site_url('envios/nosotros'); ?>"> Más Información</a>
                  </button>
                  <button type="button"  class="slide-btn">
                  <a href="<?php echo site_url(); ?>"> Contactanos</a>
                  </button>

                </div><!-- /.single-slide-item-content-->
              </div><!-- /.col-->
            </div><!-- /.row-->
          </div><!-- /.container-->
        </div><!-- /.single-slide-item-->
      </div><!-- /.item .active-->
      <div class="item">
        <div class="single-slide-item slide-2">
          <div class="container">
            <div class="row">
              <div class="col-sm-12">
                <div class="single-slide-item-content">
                  <h2>
                    Como Funciona <br> FedEx
                  </h2>
                  <p>
                    FedEx ofrece una variedad de servicios de envío para acomodar diferentes líneas de tiempo y presupuestos.
                  </p>
                  <button type="button"  class="slide-btn">
                    <a href="<?php echo site_url('envios/servicios'); ?>"> Más Información...</a>
                  </button>
                </div><!-- /.single-slide-item-content-->

              </div><!-- /.col-->
            </div><!-- /.row-->
          </div><!-- /.container-->
        </div><!-- /.single-slide-item-->
      </div><!-- /.item .active-->
    </div><!-- /.carousel-inner-->
