<!--service start-->
<section  class="service">
    <div class="container">
      <div class="service-details">
        <div class="section-header text-center">
          <h2>Nuestros Servicios</h2>
          <p>
            Creamos la herramienta FedEx International Shipping Assist para ayudar a que su proceso de envío internacional sea más eficiente y preciso. Las características clave incluyen: acceso instantáneo a información de envío importante, la capacidad de guardar y reutilizar códigos del Sistema Armonizado y aranceles e impuestos estimados para envíos repetidos.
          </p>
        </div><!--/.section-header-->
        <div class="service-content-one">
          <div class="row">
            <div class="col-sm-4 col-xs-12">
              <div class="service-single text-center">
                <div class="service-img">
                  <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/5b/FedEx_DC10.jpg/640px-FedEx_DC10.jpg" alt="image of service" />
                </div><!--/.service-img-->
                <div class="service-txt">
                  <h2>
                    <a href="#"><b>SERVICIO AÉREO</b></a>
                  </h2>
                  <p>
                  FedEx Logistics ofrece servicios de envíos internacionales via aérea que conectan todos los principales mercados mundiales, tanto de importación como de exportación, intercontinentales e interregionales.
                   La cobertura de nuestros servicios abarca Europa, Norteamérica, la región Asia-Pacífico, África y Oriente Medio.  </p>
                  <a href="#" class="service-btn">
                    <button type="" name="Costo">COSTO: 100 USD</button>
                  </a>
                </div><!--/.service-txt-->
              </div><!--/.service-single-->
            </div><!--/.col-->
            <div class="col-sm-4 col-xs-12">
              <div class="service-single text-center">
                <div class="service-img">
                  <img src="https://cnnespanol.cnn.com/wp-content/uploads/2019/08/180917161328-fedex-truck-super-tease.jpg?quality=100&strip=info" alt="image of service" />
                </div><!--/.service-img-->
                <div class="service-txt">
                  <h2>
                    <a href="#"><b>SERVICIO TERRESTRE</b></a>

                  </h2>
                  <p>
                  Nadie entiende de envíos internacionales mejor que FedEx. Realizar envíos a EE. UU. o al otro lado del mundo no tiene que ser complicado. FedEx te ofrece una variedad de servicios y herramientas para simplificarlo. Ya sea que envíes documentos, cajas o carga,
                   puede contar con FedEx para una entrega rápida y confiable a más de 220 destinos en todo el mundo.  </p>
                  <a href="#" class="service-btn">
                    <button type="" name="Costo">COSTO: 100 USD</button>
                  </a>
                </div><!--/.service-txt-->
              </div><!--/.service-single-->
            </div><!--/.col-->
            <div class="col-sm-4 col-xs-12">
              <div class="service-single text-center">
                <div class="service-img">
                  <img src="https://www.fedex.com/content/dam/fedex/eu-europe/Digtial-Internation-MVP/images/2020/Q3/Plane_4kopie_569066930.jpg" alt="image of service" />
                </div><!--/.service-img-->
                <div class="service-txt">
                  <h2>
                    <a href="#"><b>SERVICIO MARÍTIMO</b></a>
                  </h2>
                  <p>
                  Ofrecemos opciones que se adaptan tanto al tamaño, como a requisitos especiales, asi como al destino de sus envíos. Nuestra capacidad de carga de contenedor completo (Full-Container Load, FCL) y grupaje (Less-than-Container Load, LCL) nos permite adaptar nuestros servicios a sus necesidades. Tenemos la solución perfecta independientemente de que lo que quiera sea realizar envíos de grandes cantidades de material líquido o de que cargue mercancía de gran tamaño mediante grúas. </p>
                  <a href="#" class="service-btn">
                    <button type="" name="Costo">COSTO: 100 USD</button>
                  </a>
                </div><!--/.service-txt-->
              </div><!--/.service-single-->
            </div><!--/.col-->
          </div><!--/.row-->
        </div><!--/.service-content-one-->

      </div><!--/.service-details-->
    </div><!--/.container-->

</section><!--/.service-->
<!--service end-->

<!--team start -->
<section id="team" class="team  team-main">
  <div class="container">
    <div class="team-details">
      <div class="project-header team-header team-head text-center">
        <h2>Nuestro Equipo de Trabajo</h2>
        <p>
          Fedex cuenta con personal capacitado para realizar envios dentro y fuera de las ciudades.
        </p>
      </div><!--/.project-header-->
      <div class="team-card">
        <div class="container">
          <div class="row">
            <div class="owl-carousel  team-carousel">
              <div class="col-sm-3 col-xs-12">
                <div class="single-team-box single-team-card team-box-bg-1">
                  <div class="team-box-inner">
                    <h3>tom hanks</h3>
                    <p class="team-meta">Founder &  CEO</p>

                  </div><!--/.team-box-inner-->

                </div><!--/.single-team-box-->
              </div><!--.col-->
              <div class="col-sm-3 col-xs-12">
                <div class="single-team-box single-team-card team-box-bg-2">
                  <div class="team-box-inner">
                    <h3>alex browne</h3>
                    <p class="team-meta">
                      Director, Management & Research
                    </p>

                  </div><!--/.team-box-inner-->
                </div><!--/.single-team-box-->
              </div><!--.col-->
              <div class="col-sm-3 col-xs-12">
                <div class="single-team-box single-team-card team-box-bg-3">
                  <div class="team-box-inner">
                    <h3>darren j. stevens</h3>
                    <p class="team-meta">
                      Director, Finance Solution
                    </p>


                  </div><!--/.team-box-inner-->
                </div><!--/.single-team-box-->
              </div><!--.col-->
              <div class="col-sm-3 col-xs-12">
                <div class="single-team-box single-team-card team-box-bg-4">
                  <div class="team-box-inner">
                    <h3>kevin thomson</h3>
                    <p class="team-meta">
                      Head, Legal Advising
                    </p>

                  </div><!--/.team-box-inner-->
                </div><!--/.single-team-box-->
              </div><!--.col-->
            </div><!--/.team-carousel-->
          </div><!--/.row-->
        </div><!--/.container-->
      </div><!--/.team-card-->
      <div class="team-card team-mrt-70">
        <div class="container">
          <div class="row">
            <div class="owl-carousel  team-carousel">
              <div class="col-sm-3 col-xs-12">
                <div class="single-team-box single-team-card team-box-bg-4">
                  <div class="team-box-inner">
                    <h3>tom hanks</h3>
                    <p class="team-meta">Founder &  CEO</p>

                  </div><!--/.team-box-inner-->

                </div><!--/.single-team-box-->
              </div><!--.col-->
              <div class="col-sm-3 col-xs-12">
                <div class="single-team-box single-team-card team-box-bg-3">
                  <div class="team-box-inner">
                    <h3>alex browne</h3>
                    <p class="team-meta">
                      Director, Management & Research
                    </p>

                  </div><!--/.team-box-inner-->
                </div><!--/.single-team-box-->
              </div><!--.col-->
              <div class="col-sm-3 col-xs-12">
                <div class="single-team-box single-team-card team-box-bg-2">
                  <div class="team-box-inner">
                    <h3>darren j. stevens</h3>
                    <p class="team-meta">
                      Director, Finance Solution
                    </p>

                  </div><!--/.team-box-inner-->
                </div><!--/.single-team-box-->
              </div><!--.col-->
              <div class="col-sm-3 col-xs-12">
                <div class="single-team-box single-team-card team-box-bg-1">
                  <div class="team-box-inner">
                    <h3>kevin thomson</h3>
                    <p class="team-meta">
                      Head, Legal Advising
                    </p>

                  </div><!--/.team-box-inner-->
                </div><!--/.single-team-box-->
              </div><!--.col-->
            </div><!--/.team-carousel-->
          </div><!--/.row-->
        </div><!--/.container-->
      </div><!--/.team-card-->
    </div><!--/.team-details-->
  </div><!--/.container-->

</section><!--/.team-->
<!--team end-->
