<!--about-history start-->
<div class="about-history">
  <div class="container">
    <div class="about-history-content">

      <div class="row">

        <div class="col-md-5 col-sm-12">
          <div class="single-about-history">
            <div class="about-history-img">
              <img src="https://www.fedex.com/content/dam/fedex/eu-europe/Digtial-Internation-MVP/images/2019/Q4/EU_EU_2019_5_web_jpg_NA_1973_memphis_marketing_71334311.jpg" alt="about">
            </div><!--/.about-history-img-->
          </div><!--/.single-about-history-->
        </div><!--/.col-->

        <div class="col-md-offset-1 col-md-6 col-sm-12">
          <div class="single-about-history">
            <div class="about-history-txt">
              <h2>Historia de Nuestra Empresa</h2>
              <p class="text-justify">
                La innovación propulsó el despegue de FedEx, y continúa sirviendo de combustible para nuestro éxito.
                Nuestra historia se remonta a 1965, cuando nuestro director ejecutivo y Presidente Frederick W. Smith, por aquel entonces estudiante de Yale, diseñó un moderno sistema nuevo que permitiría una entrega segura de los envíos más urgentes..
                FedEx Express comenzó sus servicios en Europa en 1984 y, actualmente, presta servicio en cientos de ciudades y mercados con su entrega exprés intercontinental e internacional.
              </p>

              <div class="main-timeline">

                <div class="row">
                  <div class="col-md-2 col-sm-2">
                    <div class="experience-time">
                      <h3>1965</h3>
                    </div><!--/.experience-time-->
                  </div><!--/.col-->
                  <div class="col-md-10 col-sm-10">
                    <div class="timeline">

                      <div class="timeline-content">
                        <h4 class="title"> <span><i class="fa fa-circle-o" aria-hidden="true"></i></span></h4>

                        <ul class="description">
                          <li> Un sistema adaptado a envíos urgentes y cuya puntualidad es importante, como medicamentos, piezas de ordenadores y dispositivos electrónicos. Recibe una nota media.</li>

                        </ul>
                      </div><!--/.timeline-content-->
                    </div><!--/.timeline-->
                  </div><!--/.col-->
                </div><!--/.row-->

                <div class="row">
                  <div class="col-md-2 col-sm-2">
                    <div class="experience-time">
                      <h3>1971</h3>
                    </div><!--/.experience-time-->
                  </div><!--/.col-->
                  <div class="col-md-10 col-sm-10">
                    <div class="timeline">

                      <div class="timeline-content">
                        <h4 class="title"> <span><i class="fa fa-circle-o" aria-hidden="true"></i></span></h4>

                        <ul class="description">
                          <li>Frederick W. Smith funda Federal Express Corp. en Little Rock, Arkansas.</li>
                        </ul>
                      </div><!--/.timeline-content-->
                    </div><!--/.timeline-->
                  </div><!--/.col-->
                </div><!--/.row-->

                <div class="row">
                  <div class="col-md-2 col-sm-2">
                    <div class="experience-time">
                      <h3>1973</h3>
                    </div><!--/.experience-time-->
                  </div><!--/.col-->
                  <div class="col-md-10 col-sm-10">
                    <div class="timeline">

                      <div class="timeline-content">
                        <h4 class="title"> <span><i class="fa fa-circle-o" aria-hidden="true"></i></span></h4>

                        <ul class="description">
                          <li>La primera noche de funcionamiento continuo, 389 miembros del equipo de Federal Express y 14 aviones Dassault Falcon entregan 186 paquetes al día siguiente en 25 ciudades de EE. UU., y así es como nace el moderno sector de envíos exprés por aire y tierra.</li>

                        </ul>
                      </div><!--/.timeline-content-->
                    </div><!--/.timeline-->
                  </div><!--/.col-->
                </div><!--/.row-->

                <div class="row">
                  <div class="col-md-2 col-sm-2">
                    <div class="experience-time">
                      <h3>1977</h3>
                    </div><!--/.experience-time-->
                  </div><!--/.col-->
                  <div class="col-md-10 col-sm-10">
                    <div class="timeline">

                      <div class="timeline-content">
                        <h4 class="title"> <span><i class="fa fa-circle-o" aria-hidden="true"></i></span></h4>

                        <ul class="description">
                          <li>Federal Express compra siete aviones Boeing 727, cada uno con una capacidad de carga de más de 18.000 kg, casi siete veces más que los Dassault Falcon.</li>
                        </ul>
                      </div><!--/.timeline-content-->
                    </div><!--/.timeline-->
                  </div><!--/.col-->
                </div><!--/.row-->

              </div><!--.main-timeline-->







            </div><!--/.about-history-txt-->
          </div><!--/.single-about-history-->
        </div><!--/.col-->

      </div><!--/.row-->






      <div class="row">

        <div class="col-md-5 col-sm-12">
          <div class="single-about-history">
            <div class="about-history-img">
              <img src="https://s21.q4cdn.com/665674268/files/images/timeline/Old-logo-on-truck.jpg" alt="about">
            </div><!--/.about-history-img-->
          </div><!--/.single-about-history-->
        </div><!--/.col-->

        <div class="col-md-offset-1 col-md-6 col-sm-12">
          <div class="single-about-history">
            <div class="about-history-txt">

              <div class="main-timeline">

                <div class="row">
                  <div class="col-md-2 col-sm-2">
                    <div class="experience-time">
                      <h3>1981</h3>
                    </div><!--/.experience-time-->
                  </div><!--/.col-->
                  <div class="col-md-10 col-sm-10">
                    <div class="timeline">

                      <div class="timeline-content">
                        <h4 class="title"> <span><i class="fa fa-circle-o" aria-hidden="true"></i></span></h4>

                        <ul class="description">
                          <li> Federal Express abre un primer supercentro junto al aeropuerto internacional de Memphis.</li>

                        </ul>
                      </div><!--/.timeline-content-->
                    </div><!--/.timeline-->
                  </div><!--/.col-->
                </div><!--/.row-->

                <div class="row">
                  <div class="col-md-2 col-sm-2">
                    <div class="experience-time">
                      <h3>1984</h3>
                    </div><!--/.experience-time-->
                  </div><!--/.col-->
                  <div class="col-md-10 col-sm-10">
                    <div class="timeline">

                      <div class="timeline-content">
                        <h4 class="title"> <span><i class="fa fa-circle-o" aria-hidden="true"></i></span></h4>

                        <ul class="description">
                          <li>Federal Express adquiere Gelco Express International, un mensajero internacional con servicio en 84 países y territorios.</li>
                        </ul>
                      </div><!--/.timeline-content-->
                    </div><!--/.timeline-->
                  </div><!--/.col-->
                </div><!--/.row-->

                <div class="row">
                  <div class="col-md-2 col-sm-2">
                    <div class="experience-time">
                      <h3>1990</h3>
                    </div><!--/.experience-time-->
                  </div><!--/.col-->
                  <div class="col-md-10 col-sm-10">
                    <div class="timeline">

                      <div class="timeline-content">
                        <h4 class="title"> <span><i class="fa fa-circle-o" aria-hidden="true"></i></span></h4>

                        <ul class="description">
                          <li>Federal Express comienza su servicio internacional de envíos en Polonia a través de alianzas con empresas locales.</li>

                        </ul>
                      </div><!--/.timeline-content-->
                    </div><!--/.timeline-->
                  </div><!--/.col-->
                </div><!--/.row-->

                <div class="row">
                  <div class="col-md-2 col-sm-2">
                    <div class="experience-time">
                      <h3>1995</h3>
                    </div><!--/.experience-time-->
                  </div><!--/.col-->
                  <div class="col-md-10 col-sm-10">
                    <div class="timeline">

                      <div class="timeline-content">
                        <h4 class="title"> <span><i class="fa fa-circle-o" aria-hidden="true"></i></span></h4>

                        <ul class="description">
                          <li>FedEx obtiene la certificación ISO 9001 por sus operaciones globales: el primer transportista de primer nivel
que recibe este estándar de calidad.</li>
                        </ul>
                      </div><!--/.timeline-content-->
                    </div><!--/.timeline-->
                  </div><!--/.col-->
                </div><!--/.row-->
              </div><!--.main-timeline-->
            </div><!--/.about-history-txt-->
          </div><!--/.single-about-history-->
        </div><!--/.col-->

      </div><!--/.row-->

      <div class="row">

        <div class="col-md-5 col-sm-12">
          <div class="single-about-history">
            <div class="about-history-img">
              <img src="https://revistamagazzine.com/wp-content/uploads/2022/11/FedEx-es-aliado-ideal-para-la-temporada-alta-de-envios-fa.jpg" alt="about">
            </div><!--/.about-history-img-->
          </div><!--/.single-about-history-->
        </div><!--/.col-->

        <div class="col-md-offset-1 col-md-6 col-sm-12">
          <div class="single-about-history">
            <div class="about-history-txt">

              <div class="main-timeline">

                <div class="row">
                  <div class="col-md-2 col-sm-2">
                    <div class="experience-time">
                      <h3>2002</h3>
                    </div><!--/.experience-time-->
                  </div><!--/.col-->
                  <div class="col-md-10 col-sm-10">
                    <div class="timeline">

                      <div class="timeline-content">
                        <h4 class="title"> <span><i class="fa fa-circle-o" aria-hidden="true"></i></span></h4>

                        <ul class="description">
                          <li> FedEx comienza sus operaciones en Finlandia, Suecia y Dinamarca.</li>

                        </ul>
                      </div><!--/.timeline-content-->
                    </div><!--/.timeline-->
                  </div><!--/.col-->
                </div><!--/.row-->

                <div class="row">
                  <div class="col-md-2 col-sm-2">
                    <div class="experience-time">
                      <h3>2005</h3>
                    </div><!--/.experience-time-->
                  </div><!--/.col-->
                  <div class="col-md-10 col-sm-10">
                    <div class="timeline">

                      <div class="timeline-content">
                        <h4 class="title"> <span><i class="fa fa-circle-o" aria-hidden="true"></i></span></h4>

                        <ul class="description">
                          <li>FedEx el lanzamiento de un vuelo directo de Colonia a Memphis aumenta la capacidad de carga de Europa a EE. UU. en un 20 %.</li>
                        </ul>
                      </div><!--/.timeline-content-->
                    </div><!--/.timeline-->
                  </div><!--/.col-->
                </div><!--/.row-->

                <div class="row">
                  <div class="col-md-2 col-sm-2">
                    <div class="experience-time">
                      <h3>2007</h3>
                    </div><!--/.experience-time-->
                  </div><!--/.col-->
                  <div class="col-md-10 col-sm-10">
                    <div class="timeline">

                      <div class="timeline-content">
                        <h4 class="title"> <span><i class="fa fa-circle-o" aria-hidden="true"></i></span></h4>

                        <ul class="description">
                          <li>FedEx Express adquiere la compañía de transporte exprés húngara
                            Flying Cargo Hungary Kft.</li>

                        </ul>
                      </div><!--/.timeline-content-->
                    </div><!--/.timeline-->
                  </div><!--/.col-->
                </div><!--/.row-->

                <div class="row">
                  <div class="col-md-2 col-sm-2">
                    <div class="experience-time">
                      <h3>2010</h3>
                    </div><!--/.experience-time-->
                  </div><!--/.col-->
                  <div class="col-md-10 col-sm-10">
                    <div class="timeline">

                      <div class="timeline-content">
                        <h4 class="title"> <span><i class="fa fa-circle-o" aria-hidden="true"></i></span></h4>

                        <ul class="description">
                          <li>FedEx Express lanza una importante nueva conexión entre Asia y Europa, con un vuelo directo de ida y vuelta que opera cinco días a la semana entre Hong Kong y París. El primer proveedor que ofrece un servicio de entrega al siguiente día laborable de Hong Kong a Europa.</li>
                        </ul>
                      </div><!--/.timeline-content-->
                    </div><!--/.timeline-->
                  </div><!--/.col-->
                </div><!--/.row-->
              </div><!--.main-timeline-->
            </div><!--/.about-history-txt-->
          </div><!--/.single-about-history-->
        </div><!--/.col-->

      </div><!--/.row-->

      <div class="row">

        <div class="col-md-5 col-sm-12">
          <div class="single-about-history">
            <div class="about-history-img">
              <img src="https://www.fedex.com/content/dam/fedex/eu-europe/Digtial-Internation-MVP/images/2019/Q4/EU_EU_2019_5_web_jpg_NA_2009_major_expansion_marketing_1801290722.jpg" alt="about">
            </div><!--/.about-history-img-->
          </div><!--/.single-about-history-->
        </div><!--/.col-->

        <div class="col-md-offset-1 col-md-6 col-sm-12">
          <div class="single-about-history">
            <div class="about-history-txt">

              <div class="main-timeline">

                <div class="row">
                  <div class="col-md-2 col-sm-2">
                    <div class="experience-time">
                      <h3>2014</h3>
                    </div><!--/.experience-time-->
                  </div><!--/.col-->
                  <div class="col-md-10 col-sm-10">
                    <div class="timeline">

                      <div class="timeline-content">
                        <h4 class="title"> <span><i class="fa fa-circle-o" aria-hidden="true"></i></span></h4>

                        <ul class="description">
                          <li> FedEx marca un hito en el crecimiento en Europa al abrir su estación número 100 en Sevilla, al sur de España. </li>

                        </ul>
                      </div><!--/.timeline-content-->
                    </div><!--/.timeline-->
                  </div><!--/.col-->
                </div><!--/.row-->

                <div class="row">
                  <div class="col-md-2 col-sm-2">
                    <div class="experience-time">
                      <h3>2016</h3>
                    </div><!--/.experience-time-->
                  </div><!--/.col-->
                  <div class="col-md-10 col-sm-10">
                    <div class="timeline">

                      <div class="timeline-content">
                        <h4 class="title"> <span><i class="fa fa-circle-o" aria-hidden="true"></i></span></h4>

                        <ul class="description">
                          <li>FedEx Express abre una nueva base de operaciones en Malpensa, Italia, triplicando el tamaño de las instalaciones actuales de FedEx Express de Malpensa para convertirlas en el tercer centro de Europa, tras París y Colonia.</li>
                        </ul>
                      </div><!--/.timeline-content-->
                    </div><!--/.timeline-->
                  </div><!--/.col-->
                </div><!--/.row-->

                <div class="row">
                  <div class="col-md-2 col-sm-2">
                    <div class="experience-time">
                      <h3>2020</h3>
                    </div><!--/.experience-time-->
                  </div><!--/.col-->
                  <div class="col-md-10 col-sm-10">
                    <div class="timeline">

                      <div class="timeline-content">
                        <h4 class="title"> <span><i class="fa fa-circle-o" aria-hidden="true"></i></span></h4>

                        <ul class="description">
                          <li>FedEx Express pone en marcha un proyecto piloto en seis grandes ciudades europeas con bicicletas eléctricas de carga para las entregas de última milla, y sigue evaluando las entregas sostenibles de última milla en 25 capitales de toda Europa.</li>

                        </ul>
                      </div><!--/.timeline-content-->
                    </div><!--/.timeline-->
                  </div><!--/.col-->
                </div><!--/.row-->

                <div class="row">
                  <div class="col-md-2 col-sm-2">
                    <div class="experience-time">
                      <h3>2022</h3>
                    </div><!--/.experience-time-->
                  </div><!--/.col-->
                  <div class="col-md-10 col-sm-10">
                    <div class="timeline">

                      <div class="timeline-content">
                        <h4 class="title"> <span><i class="fa fa-circle-o" aria-hidden="true"></i></span></h4>

                        <ul class="description">
                          <li>FedEx Express y TNT reúnen sus redes aéreas intraeuropeas y crean una única red aérea en Europa. Esto marca la plena integración física de FedEx y TNT en Europa. París-Charles de Gaulle es el principal centro de operaciones europeo, que conecta universalmente todos los puntos de vuelo europeos y con todos los demás continentes. El centro de operaciones de Lieja (Bélgica) conecta grandes mercados europeos específicos y varios destinos intercontinentales importantes.</li>
                        </ul>
                      </div><!--/.timeline-content-->
                    </div><!--/.timeline-->
                  </div><!--/.col-->
                </div><!--/.row-->
              </div><!--.main-timeline-->
            </div><!--/.about-history-txt-->
          </div><!--/.single-about-history-->
        </div><!--/.col-->

      </div><!--/.row-->
  </div>
  </div>
</div>
