
		<!--about-history start-->
		<div class="about-history">
			<div class="container">
				<div class="about-history-content">

					<div class="row">

						<div class="col-md-5 col-sm-12">
							<div class="single-about-history">
								<div class="about-history-img">
									<img src="https://fedex-dims.brightspotgocdn.com/dims4/default/05f7f8a/2147483647/strip/true/crop/1000x563+0+23/resize/1000x563!/quality/90/?url=https%3A%2F%2Ffedex-static.brightspotgocdn.com%2Fb6%2Fe1%2F70829f801efa58871dd5d4b1fc20%2Fp1090631-r.jpg" alt="about" height="200px" width="600px">
								</div><!--/.about-history-img-->
							</div><!--/.single-about-history-->
						</div><!--/.col-->

						<div class="col-md-offset-1 col-md-6 col-sm-12">
							<div class="single-about-history">
								<div class="about-history-txt">
									<h2>Quienes Somos</h2>
									<p>
										Creemos que una sociedad diversa es una sociedad más fuerte. Nos esforzamos por
                     asegurar que nuestra compañía refleje las muchas culturas que hay en
                    nuestra fuerza de trabajo, nuestros clientes y nuestras comunidades alrededor del mundo.	</p>

                    <p>En la semana FedEx Cares, los miembros del equipo de FedEx han participado en sus comunidades en países como Canadá, España, Francia, Italia, México, Brasil, Australia y Hong Kong.</p>
									<div class="main-timeline">








									</div><!--.main-timeline-->
								</div><!--/.about-history-txt-->
							</div><!--/.single-about-history-->
						</div><!--/.col-->

					</div><!--/.row-->
					<div class="row">
						<div class="about-vission-content">
							<div class="col-md-6 col-sm-12">
								<div class="single-about-history">
									<div class="about-history-txt">
										<h2>Misión</h2>
										<p>
										Al conectar a las personas mediante mercancías, servicios, ideas y tecnologías creamos oportunidades que impulsan la innovación, dan energía a las empresas y elevan las comunidades a niveles de vida más altos. En FedEx, creemos que
                     un mundo conectado es mejor, por lo que esta creencia nos guía en todo lo que hacemos.			</p>

                     <h2>Visión</h2>
 										<p>
 										Al conectar a las personas mediante mercancías, servicios, ideas y tecnologías creamos oportunidades que impulsan la innovación, dan energía a las empresas y elevan las comunidades a niveles de vida más altos. En FedEx, creemos que
                      un mundo conectado es mejor, por lo que esta creencia nos guía en todo lo que hacemos.			</p>

										<div class="main-timeline  xtra-timeline">

											<div class="row">
												<div class="col-sm-12">
													<div class="timeline timeline-ml-20">

														<div class="timeline-content">
															<h4 class="title"> <span><i class="fa fa-circle-o" aria-hidden="true"></i></span></h4>

															<ul class="description">
																<li>Ingresos Anuales (FedEx Corp.): 35 mil millones de dólares.</li>

															</ul>
														</div><!--/.timeline-content-->
													</div><!--/.timeline-->
												</div><!--/.col-->
											</div><!--/.row-->

											<div class="row">
												<div class="col-sm-12">
													<div class="timeline timeline-ml-20">

														<div class="timeline-content">
															<h4 class="title"> <span><i class="fa fa-circle-o" aria-hidden="true"></i></span></h4>

															<ul class="description">
																<li>Empleados: más de 140 mil a nivel internacional.</li>

															</ul>
														</div><!--/.timeline-content-->
													</div><!--/.timeline-->
												</div><!--/.col-->
											</div><!--/.row-->

											<div class="row">
												<div class="col-sm-12">
													<div class="timeline timeline-ml-20">

														<div class="timeline-content">
															<h4 class="title"> <span><i class="fa fa-circle-o" aria-hidden="true"></i></span></h4>

															<ul class="description">
																<li>Promedio de transmisiones electrónicas: cerca de 63 millones cada día.</li>

															</ul>
														</div><!--/.timeline-content-->
													</div><!--/.timeline-->
												</div><!--/.col-->
											</div><!--/.row-->
										</div><!--.main-timeline-->
									</div><!--/.about-history-txt-->
								</div><!--/.single-about-history-->
							</div><!--/.col-->

							<div class="col-md-offset-1 col-md-5 col-sm-12">
								<div class="single-about-history">
									<div class="about-history-img">
										<img src="https://eldiariony.com/wp-content/uploads/sites/2/2023/02/FedEx-shutterstock_762228517.jpg?quality=75&strip=all&w=1200" alt="about">
									</div><!--/.about-history-img-->
								</div><!--/.single-about-history-->
							</div><!--/.col-->
						</div><!--/.about-vission-content-->
					</div><!--/.row-->
				</div><!--/.about-history-content-->
			</div><!--/.container-->

		</div><!--/.about-history-->
		<!--about-history end-->
