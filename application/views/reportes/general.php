
<br>
<body style="background-color:#93FEC1">

<div class="container">
<div class="row">
  <div class="col-md-10">
     &nbsp; &nbsp; &nbsp; <h1 class="text-center"> <b>UBICACIÓN DE TODOS REPORTES DE FEDEX </b> </h1>
  </div>
    <div class="col-md-2">
      <p class="text-right"> <img src="<?php echo base_url('assets/images/presi.png') ?>" height="100px" alt=""> </p>
    </div>
  </div>
</div>
<br>

		<div class="container">
			<div class="row">
        <div class="col-md-4">
          <br>
          <br>
          <br>
        <h3><i> <img src="<?php echo base_url('assets/images/cliente.png') ?>" alt=""> CLIENTES </i></h3>

        <h3><i> <img src="<?php echo base_url('assets/images/pedi.png') ?>" alt=""> PEDIDOS </i></h3>

        <h3><i> <img src="<?php echo base_url('assets/images/sucur.png') ?>" alt=""> SUCURSALES </i></h3>
        </div>
				<div class="col-md-8">
					<div id="mapaUbicacion" style="height:500px; width:100%; border:2px solid black;">
						<script type="text/javascript">
							function initMap(){
								var centro=new google.maps.LatLng(-0.9330512724983058, -78.61439822198312);
								var mapa1=new google.maps.Map(
									document.getElementById('mapaUbicacion'),
									{
										center:centro,
										zoom:6,
										mapTypeId:google.maps.MapTypeId.ROADMAP
									}
								);
                <?php if($clientemapa): ?>
                <?php foreach($clientemapa as $lugarTemporal): ?>
                var coordenadaTemporal=new google.maps.LatLng(<?php echo $lugarTemporal->latitud_cli; ?>, <?php echo $lugarTemporal->longitud_cli; ?>);
                var marcador=new google.maps.Marker({
                  position:coordenadaTemporal,

                  title:"<?php echo $lugarTemporal->id_cli; ?>: <?php echo $lugarTemporal->nombre_cli; ?> <?php echo $lugarTemporal->apellido_cli; ?>",
                  icon:"<?php echo base_url('assets/images/cliente.png'); ?>",
                  map:mapa1
                });
                 <?php endforeach; ?>
         			 <?php endif; ?>

               <?php if($pedidomapa): ?>
               <?php foreach($pedidomapa as $lugarTemporal): ?>
               var coordenadaTemporal=new google.maps.LatLng(<?php echo $lugarTemporal->latitud_ped; ?>, <?php echo $lugarTemporal->longitud_ped; ?>);
               var marcador=new google.maps.Marker({
                 position:coordenadaTemporal,

                 title:"<?php echo $lugarTemporal->id_ped; ?>: <?php echo $lugarTemporal->nombre_ped; ?> ",
                 icon:"<?php echo base_url('assets/images/pedi.png'); ?>",
                 map:mapa1
               });
                <?php endforeach; ?>
        			 <?php endif; ?>

               <?php if($sucursalmapa): ?>
               <?php foreach($sucursalmapa as $lugarTemporal): ?>
               var coordenadaTemporal=new google.maps.LatLng(<?php echo $lugarTemporal->latitud_suc; ?>, <?php echo $lugarTemporal->longitud_suc; ?>);
               var marcador=new google.maps.Marker({
                 position:coordenadaTemporal,

                 title:"<?php echo $lugarTemporal->id_suc; ?>: <?php echo $lugarTemporal->nombre_suc; ?> ",
                 icon:"<?php echo base_url('assets/images/sucur.png'); ?>",
                 map:mapa1
               });
                <?php endforeach; ?>
        			 <?php endif; ?>


							}//CIERRE DE LA CLASE
						</script>
					</div>
				</div>
			</div>
		</div>
    <br><br><br>
</body>
