<br>
<body style="background-color:white">

<div class="container">
<div class="row">
  <div class="col-md-10">
     &nbsp; &nbsp; &nbsp; <h1 class="text-center"> <b>UBICACIÓN DE LOS CLIENTES </b> </h1>
  </div>
    <div class="col-md-2">
      <p class="text-right"> <img src="<?php echo base_url('assets/images/cli.png'); ?>" height="100px" alt=""> </p>
    </div>
  </div>
</div>
<br>
<div class="container">
<div class="row">
  <div class="col-md-12">
    <div id="mapaUbicacion" style="height:500px;width:100%;border:2px solid black;">

    </div>

  </div>

</div>
</div>

  <script type="text/javascript">
     function initMap(){
       var centro=new google.maps.LatLng(-0.9103118368246511,-78.6288056178432);

       var mapa1=new google.maps.Map(
          document.getElementById('mapaUbicacion'),
          {
            center:centro,
            zoom:7,
            mapTypeId:google.maps.MapTypeId.ROADMAP
          }
       );
       <?php if($clientemapa): ?>
       <?php foreach($clientemapa as $lugarTemporal): ?>
       var coordenadaTemporal=new google.maps.LatLng(<?php echo $lugarTemporal->latitud_cli; ?>, <?php echo $lugarTemporal->longitud_cli; ?>);
       var marcador=new google.maps.Marker({
         position:coordenadaTemporal,

         title:"<?php echo $lugarTemporal->id_cli; ?>: <?php echo $lugarTemporal->nombre_cli; ?> <?php echo $lugarTemporal->apellido_cli; ?>",
         icon:"<?php echo base_url('assets/images/cliente.png'); ?>",
         map:mapa1
       });
        <?php endforeach; ?>
			 <?php endif; ?>


     }//cierre de la funcion
  </script>
    <br><br><br>
</body>
