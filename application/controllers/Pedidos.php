<?php

    class Pedidos extends CI_Controller
    {
        function __construct()
        {
            parent::__construct();
            //cargar un modelp
           $this->load->model('Pedido');

        }

        //Funcion que renderiza la vista index
        public function indexa(){
          $data['pedido']=$this->Pedido->obtenerTodos();

            $this->load->view('header');
            $this->load->view('pedidos/indexa',$data);
            $this->load->view('footer');
        }

        //Funcion que renderiza la vista nuevo
        public function nuevoa(){
            $this->load->view('header');
            $this->load->view('pedidos/nuevoa');
            $this->load->view('footer');
        }

        public function guardar(){
          $datosNuevosPedido=array("nombre_ped"=>$this->input->post('nombre_ped'),"fechaEnvio_ped"=>$this->input->post('fechaEnvio_ped'),
          "cantidad_ped"=>$this->input->post('cantidad_ped'),"origen_ped"=>$this->input->post('origen_ped'),"destino_ped"=> $this->input->post('destino_ped'),
          "recibido_ped"=>$this->input->post('recibido_ped'),"fechaEntrega_ped"=>$this->input->post('fechaEntrega_ped'),
          "latitud_ped"=>$this->input->post('latitud_ped'),"longitud_ped"=>$this->input->post('longitud_ped')
        );
        if($this->Pedido->insertar($datosNuevosPedido)){
           $this->session->set_flashdata("confirmacion","Pedido guardado exitosamente");
        }else{
          $this->session->set_flashdata("error","Error al guardar, intente otra vez");
        }
        redirect('pedidos/indexa');
     }
     //FUNCION PARA ELIMINAR CONSTRUCTORES
     public function eliminar($id_ped){
       if ($this->Pedido->borrar($id_ped)){
         $this->session->set_flashdata("confirmacion","Pedido eliminado exitosamente");
       } else {
         $this->session->set_flashdata("error","Error al eliminar, intente otra vez");
       }
       redirect('pedidos/indexa');
     }

     //FUNCTION RENDERIZAR VISTA EDITAR A EL REGISTRO
     public function editar($id_ped){
       $data["pedidoEditar"]=$this->Pedido->obtenerPorId($id_ped);
       $this->load->view('header');
       $this->load->view('pedidos/editar',$data);
       $this->load->view('footer');
     }
     //PROCESSO DE ACTUALIZACION
     public function procesarActualizacion(){
       $datosEditados=array("nombre_ped"=>$this->input->post('nombre_ped'),"fechaEnvio_ped"=>$this->input->post('fechaEnvio_ped'),
          "cantidad_ped"=>$this->input->post('cantidad_ped'),"origen_ped"=>$this->input->post('origen_ped'),"destino_ped"=> $this->input->post('destino_ped'),
          "recibido_ped"=>$this->input->post('recibido_ped'),"fechaEntrega_ped"=>$this->input->post('fechaEntrega_ped'),
          "latitud_ped"=>$this->input->post('latitud_ped'),"longitud_ped"=>$this->input->post('longitud_ped')
     );
     $id_ped=$this->input->post("id_ped");
     if ($this->Pedido->actualizar($id_ped,$datosEditados)) {
       $this->session->set_flashdata("confirmacion","Pedido actualizado exitosamente");
     } else {
       $this->session->set_flashdata("actualizar","Error al actualizar, intente otra vez");
     }
     redirect("pedidos/indexa");
   }

        }//cierre de la clases NOOOOOOOOOOO BORRRAARRRR

?>
