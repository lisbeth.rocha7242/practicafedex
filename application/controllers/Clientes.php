
<?php

    class Clientes extends CI_Controller
    {
        function __construct()
        {
            parent::__construct();
            //cargar un modelp
           $this->load->model('Cliente');



        }

        //Funcion que renderiza la vista index
        public function index(){
          $data['clientes']=$this->Cliente->obtenerTodos();

            $this->load->view('header');
            $this->load->view('clientes/index',$data);
            $this->load->view('footer');
        }

        //Funcion que renderiza la vista nuevo
        public function nuevo(){
            $this->load->view('header');
            $this->load->view('clientes/nuevo');
            $this->load->view('footer');
        }


        public function guardar(){
          $datosNuevosCliente=array("cedula_cli"=>$this->input->post('cedula_cli'),
          "nombre_cli"=>$this->input->post('nombre_cli'),
          "apellido_cli"=>$this->input->post('apellido_cli'),
          "email_cli"=>$this->input->post('email_cli'),
          "ciudad_cli"=> $this->input->post('ciudad_cli'),
          "pais_cli"=>$this->input->post('pais_cli'),
          "telefono_cli"=>$this->input->post('telefono_cli'),
          "latitud_cli"=>$this->input->post('latitud_cli'),
          "longitud_cli"=>$this->input->post('longitud_cli'),


        );
        if($this->Cliente->insertar($datosNuevosCliente)){
          $this->session->set_flashdata("confirmacion","Cliente guardado exitosamente");
        }else{
          $this->session->set_flashdata("error","Error al guardar, intente otra vez");
        }
        redirect('clientes/index');
     }
     public function editar($id_cli){
      $data["clienteEditar"]=$this->Cliente->obtenerPorId($id_cli);
      $this->load->view('header');
      $this->load->view('clientes/editar',$data);
      $this->load->view('footer');
    }
     //FUNCION PARA ELIMINAR CONSTRUCTORES
     public function eliminar($id_cli){
       if ($this->Cliente->borrar($id_cli)){
         $this->session->set_flashdata("confirmacion","Cliente eliminado exitosamente");
       } else {
        $this->session->set_flashdata("error","Error al guardar, intente otra vez");
       }
       redirect('clientes/index');
     }
//ACTUALIZAR
      public function procesarActualizacion() {
        $datosEditados = array(
          "cedula_cli"=>$this->input->post('cedula_cli'),
          "nombre_cli"=>$this->input->post('nombre_cli'),
          "apellido_cli"=>$this->input->post('apellido_cli'),
          "email_cli"=>$this->input->post('email_cli'),
          "ciudad_cli"=> $this->input->post('ciudad_cli'),
          "pais_cli"=>$this->input->post('pais_cli'),
          "telefono_cli"=>$this->input->post('telefono_cli'),
          "latitud_cli"=>$this->input->post('latitud_cli'),
          "longitud_cli"=>$this->input->post('longitud_cli'),
        );
        $id_cli =$this->input->post('id_cli');
        if ($this->Cliente->actualizar($id_cli, $datosEditados)) {
<<<<<<< HEAD
          $this->session->set_flashdata("confirmacion","Cliente editado exitosamente");
        } else {
            $this->session->set_flashdata("error","Error al guardar, intente otra vez");
        }
        redirect("clientes/index");
=======
          redirect("clientes/index");
        } else {
          echo "ERROR AL ACTUALIZAR :(";
        }
>>>>>>> 9935d28440230108117e3b01a0ceaf71770dafea
      }

    }//cierre de la clases NOOOOOOOOOOO BORRRAARRRR

?>
