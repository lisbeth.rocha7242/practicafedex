<?php

    class Sucursales extends CI_Controller
    {
        function __construct()
        {
            parent::__construct();
            //cargar un modelp
           $this->load->model('Sucursal');

        }

        //Funcion que renderiza la vista index
        public function indexc(){
          $data['sucursal']=$this->Sucursal->obtenerTodos();

            $this->load->view('header');
            $this->load->view('sucursales/indexc',$data);
            $this->load->view('footer');
        }

        //Funcion que renderiza la vista nuevo
        public function nuevoc(){
            $this->load->view('header');
            $this->load->view('sucursales/nuevoc');
            $this->load->view('footer');
        }

        public function guardar(){
          $datosNuevosSucursal=array("nombre_suc"=>$this->input->post('nombre_suc'),"calle_suc"=>$this->input->post('calle_suc'),
          "piso_suc"=>$this->input->post('piso_suc'),"telefono_suc"=>$this->input->post('telefono_suc'),"continente_suc"=> $this->input->post('continente_suc'),
          "encargado_suc"=>$this->input->post('encargado_suc'),"latitud_suc"=>$this->input->post('latitud_suc'),"longitud_suc"=>$this->input->post('longitud_suc')
        );
        if($this->Sucursal->insertar($datosNuevosSucursal)){
           $this->session->set_flashdata("confirmacion","Sucursal guardado exitosamente");
        }else{
          $this->session->set_flashdata("error","Error al guardar, intente otra vez");
        }
        redirect('sucursales/indexc');
     }
     //FUNCION PARA ELIMINAR CONSTRUCTORES
     public function eliminar($id_suc){
       if ($this->Sucursal->borrar($id_suc)){
         $this->session->set_flashdata("confirmacion","Sucursal eliminado exitosamente");
       } else {
         $this->session->set_flashdata("error","Error al eliminar, intente otra vez");
       }
       redirect('sucursales/indexc');
     }

     //FUNCTION RENDERIZAR VISTA EDITAR A EL REGISTRO
     public function editar($id_suc){
       $data["sucursalEditar"]=$this->Sucursal->obtenerPorId($id_suc);
       $this->load->view('header');
       $this->load->view('sucursales/editar',$data);
       $this->load->view('footer');
     }
     //PROCESSO DE ACTUALIZACION
     public function procesarActualizacion(){
       $datosEditados=array("nombre_suc"=>$this->input->post('nombre_suc'),"calle_suc"=>$this->input->post('calle_suc'),
       "piso_suc"=>$this->input->post('piso_suc'),"telefono_suc"=>$this->input->post('telefono_suc'),"continente_suc"=> $this->input->post('continente_suc'),
       "encargado_suc"=>$this->input->post('encargado_suc'),"latitud_suc"=>$this->input->post('latitud_suc'),"longitud_suc"=>$this->input->post('longitud_suc')
     );
     $id_suc=$this->input->post("id_suc");
     if ($this->Sucursal->actualizar($id_suc,$datosEditados)) {
       $this->session->set_flashdata("confirmacion","Sucursal actualizado exitosamente");
     } else {
       $this->session->set_flashdata("actualizar","Error al actualizar, intente otra vez");
     }
     redirect("sucursales/indexc");
   }

        }//cierre de la clases NOOOOOOOOOOO BORRRAARRRR

?>
