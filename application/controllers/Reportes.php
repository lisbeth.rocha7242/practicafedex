<?php
/**
 *
 */
class Reportes extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->model('Reporte');

  }

  public function clientes(){
    $data["clientemapa"]=$this->Reporte->obtenerTodosCliente();
    $this->load->view('header');
    $this->load->view('reportes/clientes',$data);
    $this->load->view('footer');
  }

  public function pedidos(){
    $data["pedidomapa"]=$this->Reporte->obtenerTodosPedido();
    $this->load->view('header');
    $this->load->view('reportes/pedidos',$data);
    $this->load->view('footer');
  }
  public function sucursales(){
    $data["sucursalmapa"]=$this->Reporte->obtenerTodosSucursal();
    $this->load->view('header');
    $this->load->view('reportes/sucursales',$data);
    $this->load->view('footer');
  }
  public function general(){
    $data["clientemapa"]=$this->Reporte->obtenerTodosCliente();
    $data["pedidomapa"]=$this->Reporte->obtenerTodosPedido();
    $data["sucursalmapa"]=$this->Reporte->obtenerTodosSucursal();
    $this->load->view('header');
    $this->load->view('reportes/general',$data);
    $this->load->view('footer');
  }
}//ciere de la clase

?>
