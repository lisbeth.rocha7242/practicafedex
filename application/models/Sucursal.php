<?php
class Sucursal extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }
  //funcion para insertar un instructor un instructor en mysql
  function insertar($datos){

  return $this->db->insert("sucursal",$datos);
  }
  //FUNCION PARA CONSULTAR Instructores

  function obtenerTodos(){
    $listadoSucursales=$this->db->get("sucursal");

    if($listadoSucursales->num_rows()>0){//SI HAY DATOOOOOS
      return $listadoSucursales->result();
    }else {
      return false;
    }
  }
  //BORRAR Instructores
  function borrar($id_suc){
    //DELTE FROM INSTRUCTOR WHERE id_ins
    $this->db->where("id_suc",$id_suc);
    if ($this->db->delete("sucursal")) {
      return true;
    } else {
      return false;
    }

  }

  function obtenerPorId($id_suc){
    $this->db->where("id_suc",$id_suc);
    $sucursal=$this->db->get("sucursal");
    if ($sucursal->num_rows()>0) {
      return $sucursal->row();
    }
    return false;
  }
  //FUNCION PARA ACTUALIZAR UN Instructor
  function actualizar($id_suc,$datos){
    $this->db->where("id_suc",$id_suc);
    return $this->db->update('sucursal',$datos);
  }
}//CIERRE DE LA CLASEE
 ?>
