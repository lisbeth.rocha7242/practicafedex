<?php
class Pedido extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }
  //funcion para insertar un instructor un instructor en mysql
  function insertar($datos){

  return $this->db->insert("pedido",$datos);
  }
  //FUNCION PARA CONSULTAR Instructores

  function obtenerTodos(){
    $listadoPedidos=$this->db->get("pedido");

    if($listadoPedidos->num_rows()>0){//SI HAY DATOOOOOS
      return $listadoPedidos->result();
    }else {
      return false;
    }
  }
  //BORRAR Instructores
  function borrar($id_ped){
    //DELTE FROM INSTRUCTOR WHERE id_ins
    $this->db->where("id_ped",$id_ped);
    if ($this->db->delete("pedido")) {
      return true;
    } else {
      return false;
    }

  }

  function obtenerPorId($id_ped){
    $this->db->where("id_ped",$id_ped);
    $pedido=$this->db->get("pedido");
    if ($pedido->num_rows()>0) {
      return $pedido->row();
    }
    return false;
  }
  //FUNCION PARA ACTUALIZAR UN Instructor
  function actualizar($id_ped,$datos){
    $this->db->where("id_ped",$id_ped);
    return $this->db->update('pedido',$datos);
  }
}//CIERRE DE LA CLASEE
 ?>
